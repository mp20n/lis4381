<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
	<meta name="author" content="Michael Proeber">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Skillset 15 (Write Read File)</title>
	<?php include_once("../css/include_css.php"); ?>
	
	<style>
		body
		{
			padding-top: 50px;
		}

		.starter-template
		{
			padding: 0px 15px;
			text-align: center;
		}

		h1
		{
			margin: 0px;     
			color: #9A2A2A;
			text-shadow: 2px 2px #CEB888;
			padding: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;    
		}

		h2 {
			font-weight: normal;
		}

		h3 {
			font-weight: normal;
		}

		.navbar-custom {
			background-color:#9A2A2A !important;
		}

		.navbar-custom .navbar-text {
			color: white;
		}

		.center {
			margin-left: auto;
			margin-right: auto;
			text-align: center;
		}

		th, td {
			padding: 15px;
		}
	</style>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>	
			</div>

			<h2>File Data</h2> 
			<form class="form-horizontal" role="form" method="post" action="process.php"> 
				<div class="form-group"> 
					<label class="control-label col-sm-2" for="comment">Comment: </label> 
					<div class="col-sm-10"> 
						<textarea class="form-control" rows="5" name="comment" id="comment" placeholder="Please enter text here..."></textarea> 
					</div> 
				</div> 
				<div class="form-group"> 
					<div class="col-sm-12"> 
						<button type="submit" class="btn btn-default">Submit</button> 
					</div> 
				</div> 
			</form>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>

</body>
</html>
