<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
		<meta name="author" content="Michael Proeber">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>	
		
		<style>
			body
{
  padding-top: 50px;
}

.starter-template
{
  padding: 0px 15px;
  text-align: center;
}

h1
{
	margin: 0px;     
	color: #9A2A2A;
	text-shadow: 2px 2px #CEB888;
	padding: 0px;
	font-size: 48px;
	font-family: "trebuchet ms", sans-serif;    
}

h2 {
	font-weight: normal;
}

h3 {
	font-weight: normal;
}

.navbar-custom {
	background-color:#9A2A2A !important;
}

.navbar-custom .navbar-text {
	color: white;
}

.center {
	margin-left: auto;
	margin-right: auto;
}

th, td {
	padding: 15px;
}
	</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> In this assignment, we will learn how to use a Distributed Version Control System using Bitbucket. Individually and as a team member, Git repositories can be managed locally and then pushed using command-line.
					<br>
					This course also uses Android Studio to develop Android applications.
				</p>

				<br>

				<h4>Java Installation</h4>
				<img src="img/jdk.png" class="img-responsive center-block" alt="JDK Installation">

				<br>

				<h4>Android Studio Installation</h4>
				<img src="img/android_studio.png" class="img-responsive center-block" alt="Android Studio Installation">

				<br>

				<h4>MySQL, Apache, and PHP Brew Services Installation</h4>
				<img src="img/installations.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<br>

				<h4>Bitbucket links</h4>
				<a href="https://bitbucket.org/mp20n/lis4381/src/master/">Course Repository</a> <br>
				<a href="https://bitbucket.org/mp20n/lis4381/src/master/a1/">A1 Repository Directory</a> <br>
				<a href="https://bitbucket.org/mp20n/bitbucketstationlocations/">Bitbucket Station Locations Tutorial Repository</a> <br>
				<a href="http://www.qcitr.com/usefullinks.htm#lesson3b">A1 Git Setup Tutorial</a>

				<br><br>

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
