# LIS4381 – Mobile Web App Development

## Michael Proeber

### Assignment #1 Requirements:

1. Distributed Version Control using Git and Bitbucket
2. Development installations
3. Chapter 1 and 2 questions

#### README.md file should include the following items:

* Git commands with short descriptions
* Screenshot of MySQL, Apache, and PHP running
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* Bitbucket repo links (this assignment and completed tutorial)

*Basic Git Commands*

1. git init – initializes a new local Git repository
2. git status – displays state of repository. Displays tracked and untracked files and changes
3. git add – marks file(s) for inclusion into next commit. "git add ." stages all files
4. git commit – creates snapshot of repository. Includes timestamps, authors, changes. The -m flag includes a message for the commit. Make commit messages present-tense. Ex) Fix sorting algorithm
5. git push – pushes local commits to remote repository. Ex) git push <REMOTENAME> <BRANCHNAME>
6. git pull – updates local working branch with commits from remote, and updates all remote tracking branches. Run git status before git pull
7. git grep – searches entire repo using a given keyword

#### Assignment Screenshots:

*Screenshot of PHP running http://localhost:8080:*
![PHP information page](img/php.png)

*Screenshot of MySQL, Apache, and PHP Brew services running:*
![Brew services](img/installations.png)

*Screenshot of running java Hello:*
![JDK Installation Screenshot](img/jdk.png)

*Screenshot of Android Studio - My First App:*
![Android Studio Installation Screenshot](img/android_studio.png)


#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course repository")

*Assignment directory:*
[A1 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/a1/ "A1 Repository Directory")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mp20n/bitbucketstationlocations/ "Bitbucket Station Locations repository")

*Tutorial: Setting up Distributed Version Control System:*
[A1 Git Setup](http://www.qcitr.com/usefullinks.htm#lesson3b "Git Setup Tutorial")