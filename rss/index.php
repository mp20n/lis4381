<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
		<meta name="author" content="Michael Proeber">
    <link rel="icon" href="favicon.ico">

	<title>LIS4381 - NASA RSS Feed</title>		
	<?php include_once("../css/include_css.php"); ?>	
	
	<style>
		body
		{
			padding-top: 50px;
		}

		.starter-template
		{
			padding: 0px 15px;
			text-align: center;
		}

		h1
		{
			margin: 0px;     
			color: #9A2A2A;
			text-shadow: 2px 2px #CEB888;
			padding: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;    
		}

		h2 {
			font-weight: normal;
		}

		h3 {
			font-weight: normal;
		}

		.navbar-custom {
			background-color:#9A2A2A !important;
		}

		.navbar-custom .navbar-text {
			color: white;
		}

		.center {
			margin-left: auto;
			margin-right: auto;
		}

		th, td {
			padding: 15px;
		}
	</style>
  </head>

  <body>

		<?php include_once("../global/nav.php");
		
		$html = ""; 
		$publisher = "NASA"; 
		$url = "https://www.nasa.gov/rss/dyn/breaking_news.rss"; 
		$html .= '<h2>' . $publisher . '</h2>'; 
		$html .= $url . '<br><br>'; 
		$rss = simplexml_load_file($url); 
		$count = 0; 
		$html .= '<ul>'; 
		foreach($rss->channel->item as $item) {
			$count++; 
			if ($count > 20) break; 
			$html .= '<li><a href="'.htmlspecialchars($item->link).'">'. htmlspecialchars($item->title) . '</a><br>'; 
			$html .= htmlspecialchars($item->description) . '<br>'; 
			$html .= htmlspecialchars($item->pubDate) . '</li><br>'; 
		} 
		$html .= '</ul>'; 
		print $html; ?>

		<?php include_once "global/footer.php"; ?>

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
