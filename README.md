# LIS4381 – Mobile Web App Development

## Michael Proeber

### LIS4381 Requirements:

*Course Work Links:*

- [Java skillsets](skillsets/README.md "Java skillsets README.md")

- [A1 README.md](a1/README.md "My A1 README.md file")
    - Install MySQL
    - Install Apache
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo and provide links
    - Complete Bitbucket tutorial (BitbucketStationLocations)
    - Provide Git command descriptions

- [A2 README.md](a2/README.md "My A2 README.md file")
    - Develop bruschetta recipe application
    - Skillset 1 – Even or Odd Number calculator
    - Skillset 2 – Largest of Two Integers calculator
    - Skillset 3 – Arrays and Loops demonstration
    - Chapter questions
    - Bitbucket repo links

- [A3 README.md](a3/README.md "My A3 README.md file")
    - Develop ticket calculator Android application
    - Skillset 4 – Decision Structures
    - Skillset 5 – Random Number Generator
    - Skillset 6 – Methods
    - Chapter questions
    - Entity Relationship Diagram of pet store MySQL database with data
    - Links to ERD .mwb file and SQL code
    - Bitbucket repo links

- [P1 README.md](p1/README.md "My P1 README.md file")
    - Develop Business Card application
    - Skillset 7 – Random Number Generator Data Validation
    - Skillset 8 – Largest Three Numbers
    - Skillset 9 – Array Runtime Data Validation
    - Chapter questions

- [A4 README.md](a4/README.md "My A4 README.md file")
    - Create local course web app site
    - Develop client-side Bootstrap form
    - Skillset 10 – Array List
    - Skillset 11 – Alphanumeric Special Characters
    - Skillset 12 – Temperature Conversion
    - Chapter questions

- [A5 README.md](a5/README.md "My A5 README.md file")
    - Create an index page displaying a table of the local MySQL pet store database
    - Create a page to add a pet store record to the petstore table, with server-side form validation
    - Skillset 13 – Sphere Volume Calculator
    - Skillset 14 (PHP) – Simple Calculator
    - Skillset 15 (PHP) – Write Read File
    - Chapter questions

- [P2 README.md](p2/README.md "My P2 README.md file")
    - Create a form page to edit a pet store record in the petstore table, with server-side form validation
    - Create script to process edit query
    - Create script to process delete query
    - Create an RSS feed page displaying a live feed
    - Chapter questions