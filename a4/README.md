# LIS4381 – Mobile Web App Development

## Michael Proeber

### Assignment #4 Requirements

1. Create a local course web app site that:
    - has a home index page with a carousel of at least 3 images
    - has assignment page with a header and footer for each assignment
    - has a navigation bar
    - has a form to add a petstore record with client-side validation
2. Skillset 10 – Array List
3. Skillset 11 – Alphanumeric Special Characters
4. Skillset 12 – Temperature Conversion
5. Chapter questions

#### README.md file should include the following items

- Screenshots of local web app pages including A4
- Screenshots of skillsets running
- Bitbucket repo links (course and this assignment)

#### Assignment Screenshots

##### Local Web App

![Homepage](img/home.png)

![Assignment 4](img/A4.png)

##### Java Skillsets

| [Skillset 10 – ArrayList](../skillsets/SS10_ArrayList/ "Skillset 10 link") | [Skillset 11 – AlphaSpecialCharacters](../skillsets/SS11_AlphaSpecialCharacters/ "Skillset 11 link") | [Skillset 12 – TemperatureConversion](../skillsets/SS12_TemperatureConversion/ "Skillset 12 link") |
| --------- | ------ | ------ |
| [![Skillset 10 screenshot](img/SS10.png)](img/SS10.png "Skillset 10") | [![Skillset 11 screenshot](img/SS11.png)](img/SS11.png "Skillset 11") | [![Skillset 12 screenshot](img/SS12.png)](img/SS12.png "Skillset 12") |

<br>

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course Repository")

*Assignment directory:*
[A4 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/a4/ "A4 Repository Directory")