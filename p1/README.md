# LIS4381 – Mobile Web App Development

## Michael Proeber

### Project #1 Requirements

1. Develop an application that:
    - contains your name, portrait, and details button in Main Activity
    - contains contact information and interests in Details Activity
    - has image border
    - has button text shadow
    - has launcher icon
    - has background color on both activities
2. Develop Skillset 7 – Random Number Generator With Data Validation
3. Develop Skillset 8 – Largest Three Numbers
4. Develop Skillset 9 – Array With Runtime Data Validation
5. Chapter questions

#### README.md file should include the following items

- Screenshots of both app activities
- Screenshots of skillsets running
- Bitbucket repo links (course and this assignment)

#### Project Screenshots

##### GIF demos of business card application

| Nexus 4 API 22 Lollipop | Pixel 2 API 27 Oreo |
| ----- | ------ |
| ![Nexus 4 demo using Android Lollipop](img/Nexus_4_demo.gif) | ![Pixel 2 demo using Android Oreo](img/Pixel_2_demo.gif) |

<br>

##### Java Skillsets

| [Skillset 7 – Random Number Generator With Data Validation](../skillsets/SS7_RandomNumberGeneratorDataValidation/ "Skillset 7 link") | [Skillset 8 – Largest Three Numbers Calculator](../skillsets/SS8_LargestThreeNumbers/ "Skillset 8 link") | [Skillset 9 – Array With Runtime Data Validation](../skillsets/SS9_ArrayRuntimeDataValidation/ "Skillset 9 link") |
| --------- | ------ | ------ |
| [![Skillset 7 screenshot](img/SS7.png)](img/SS7.png "Skillset 7") | [![Skillset 8 screenshot](img/SS8.png)](img/SS8.png "Skillset 8") | [![Skillset 9 screenshot](img/SS9.png)](img/SS9.png "Skillset 9") |

<br>

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course Repository")

*Project directory:*
[P1 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/p1/ "P1 Repository Directory")