<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
		<meta name="author" content="Michael Proeber">
		<link rel="icon" href="favicon.ico">

		<title>LIS4381 – My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	
		
		<!-- Carousel styles -->
		<style type="text/css">
			h2
			{
				margin: 0;     
				color: #666;
				padding-top: 50px;
				font-size: 52px;
				font-family: "trebuchet ms", sans-serif;    
			}
			.item
			{
				background: #333;    
				text-align: center;
				height: 300px !important;
			}
			.carousel
			{
				margin: 20px 0px 20px 0px;
			}
			.bs-example
			{
				margin: 20px;
			}

			body
			{
				padding-top: 50px;
			}

			.starter-template
			{
				padding: 0px 15px;
				text-align: center;
			}

			h1
			{
				margin: 0px;     
				color: #9A2A2A;
				text-shadow: 2px 2px #CEB888;
				padding: 0px;
				font-size: 48px;
				font-family: "trebuchet ms", sans-serif;    
			}

			.navbar-custom {
				background-color:#9A2A2A !important;
			}

			.navbar-custom .navbar-text {
				color: white;
			}

			.center {
				margin-left: auto;
				margin-right: auto;
			}

			th, td {
				padding: 15px;
			}
		</style>
	</head>
	<body>
		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="1000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							<div class="active item" style="background: url(img/linkedin.png) no-repeat left center; background-size: cover;">
								<div class="container">
									<div class="carousel-caption">
										<h3>LinkedIn</h3>
										<a href="https://www.linkedin.com/in/michaelproeber/">See my LinkedIn</a>
                        </div>
                      </div>
                    </div>
              
							<div class="item" style="background: url(img/bitbucket.png) no-repeat left center; background-size: cover;">
								<div class="carousel-caption">
									<h3>Bitbucket repositories</h3>
									<a href="https://bitbucket.org/mp20n/">See my Bitbucket</a>
									<!-- <img src="img/bitbucket.png" alt="Slide 2">	 -->								
								</div>
							</div>

							<div class="item" style="background: url(img/linkedin.png) no-repeat left center; background-size: cover;">
								<div class="carousel-caption">
									<h3>Third slide label</h3>
									<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
									<!-- <img src="img/slide3.png" alt="Slide 3">	 -->								
								</div>
							</div>

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
