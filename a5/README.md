# LIS4381 – Mobile Web App Development

## Michael Proeber

### Assignment #5 Requirements

1. Create an index page displaying a table of the local MySQL pet store database
2. Create a page to add a pet store record to the petstore table, with server-side form validation
3. Skillset 13 – Sphere Volume Calculator
4. Skillset 14 (PHP) – Simple Calculator
5. Skillset 15 (PHP) – Write Read File
6. Chapter questions

#### README.md file should include the following items

- Screenshots of data before and after adding a record
- Screenshots of failed form validation
- Bitbucket repo links (course and this assignment)

#### Assignment Screenshots

##### Database before adding a record

[![Before](img/A5_before.png)](img/A5_before.png)

##### Server-side form validation

[![Server-side](img/A5_error.png)](img/A5_error.png)

##### Database after adding a record

[![After](img/A5_after.png)](img/A5_after.png)

##### Java Skillsets

[Skillset 13 – Sphere Volume Calculator](../skillsets/SS13_SphereVolumeCalculator/ "Skillset 13 link")
[![Skillset 13 screenshot](img/SS13.png)](img/SS13.png "Skillset 13")

<br>

[Skillset 14 (PHP) – Simple Calculator](../simple_calculator "Skillset 14 link")
[![Skillset 14 index screenshot](img/SS14_index.png)](img/SS14_index.png "Skillset 14")
[![Skillset 14 addition screenshot](img/SS14_addition.png)](img/SS14_addition.png "Skillset 14")
[![Skillset 14 subtraction screenshot](img/SS14_subtraction.png)](img/SS14_subtraction.png "Skillset 14")
[![Skillset 14 multiplication screenshot](img/SS14_multiplication.png)](img/SS14_multiplication.png "Skillset 14")
[![Skillset 14 division screenshot](img/SS14_division.png)](img/SS14_division.png "Skillset 14")
[![Skillset 14 exponentiation screenshot](img/SS14_exponentiation.png)](img/SS14_exponentiation.png "Skillset 14")

<br>

[Skillset 15 (PHP) – Write Read File](../write_read_file "Skillset 15 link")
[![Skillset 15 index screenshot](img/SS15_index.png)](img/SS15_index.png "Skillset 15")
[![Skillset 15 result screenshot](img/SS15_result.png)](img/SS15_result.png "Skillset 15")

<br>

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course Repository")

*Assignment directory:*
[A5 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/a5/ "A5 Repository Directory")