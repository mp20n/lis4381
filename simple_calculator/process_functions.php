<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));

//exit(print_r($_POST)); //display $_POST array values from form

// or, for nicer display in browser...
/* echo "<pre>";
 * print_r($_POST);
 * echo "</pre>";
 * exit(); //stop processing, otherwise, errors below
 */

//After testing, comment out above lines.
               
//code to process inserts goes here

//include('index.php'); //forwarding is faster, one trip to server
//header('Location: index.php'); //sometimes, redirecting is needed (two trips to server)
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
	<meta name="author" content="Michael Proeber">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Skillset 14 (Simple Calculator)</title>
	<?php include_once("../css/include_css.php"); ?>
	
	<style>
		body
		{
			padding-top: 50px;
		}

		.starter-template
		{
			padding: 0px 15px;
			text-align: center;
		}

		h1
		{
			margin: 0px;     
			color: #9A2A2A;
			text-shadow: 2px 2px #CEB888;
			padding: 0px;
			font-size: 48px;
			font-family: "trebuchet ms", sans-serif;    
		}

		h2 {
			font-weight: normal;
		}

		h3 {
			font-weight: normal;
		}

		.navbar-custom {
			background-color:#9A2A2A !important;
		}

		.navbar-custom .navbar-text {
			color: white;
		}

		.center {
			margin-left: auto;
			margin-right: auto;
			text-align: center;
		}

		th, td {
			padding: 15px;
		}
	</style>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
			<div class="page-header">
				<?php include_once("global/header.php"); ?>	
			</div>

            <?php
                if (!empty($_POST)) {
                    $num1 = $_POST['num1']; 
                    $num2 = $_POST['num2']; 
                    $operation = $_POST['operation'];
                    
                    //if (preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num1) && preg_match('/^[-+]?[0-9]*\.?[0-9]+$/', $num2)) { 
                    if (is_numeric($num1) && is_numeric($num2)) {
                        echo '<h2>' . "$operation" . '</h2>'; 
                        
                        function AddNum($x, $y) { 
                            echo "$x" . " + " . "$y" . " = " . $x + $y;
                        }

                        function SubtractNum($x, $y) { 
                            echo "$x" . " - " . "$y" . " = " . $x - $y; 
                        }

                        function MultiplyNum($x, $y) { 
                            echo "$x" . " * " . "$y" . " = " . $x * $y; 
                        }

                        function DivideNum($x, $y) { 
                            if ($y == 0) echo "Cannot divide by zero!";
                            else echo "$x" . " ÷ " . "$y" . " = " . $x / $y;
                        }

                        function PowerNum($x, $y) { 
                            echo "$x" . " raised to the power of " . "$y" . " = " . pow($x, $y); 
                        }

                        if ($operation == 'addition') AddNum($num1, $num2);
                        else if ($operation == 'subtraction') SubtractNum($num1, $num2);
                        else if ($operation == 'multiplication') MultiplyNum($num1, $num2);
                        else if ($operation == 'division') DivideNum($num1, $num2);
                        else if ($operation == 'exponentiation') PowerNum($num1, $num2);
                        else echo "Must select an operation.";
                        ?>
                        <p>
                        <?php
                    }

                    else echo "Must enter valid numbers.";
                }

            else header('Location: index.php');

            ?>
            </p>
           
            <br>

            <a href="index.php">Back</a>
            
            <br><br>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 	</div> <!-- end container -->

	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
	<?php include_once("../js/include_js.php"); ?>
</body>
</html>