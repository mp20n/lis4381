import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        // display operational messages
        System.out.println("\nDeveloper: Michael Proeber");
        System.out.println("Program evaluates largest of 3 integers while checking for integers and non-numeric data.");
    }

    public static void validateUserInput() {
        Scanner sc = new Scanner(System.in);
        int num1 = 0, num2 = 0, num3 = 0;

        // prompt for 3 integers
        System.out.print("\nPlease enter 1st number: ");
        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!");
            sc.next();
            System.out.print("\nPlease try again. Enter 1st number: ");
        }
        num1 = sc.nextInt();

        System.out.print("\nPlease enter 2nd number: ");
        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!");
            sc.next();
            System.out.print("\nPlease try again. Enter 2nd number: ");
        }
        num2 = sc.nextInt();

        System.out.print("\nPlease enter 3rd number: ");
        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!");
            sc.next();
            System.out.print("\nPlease try again. Enter 3rd number: ");
        }
        num3 = sc.nextInt();

        getLargestNumber(num1, num2, num3);
    }

    public static void getLargestNumber(int num1, int num2, int num3) {
        System.out.println("\nNumbers entered: " + num1 + ", " + num2 + ", " + num3);

        if (num1 > num2 && num1 > num3)
            System.out.println("Number 1 (" + num1 + ") is largest.\n");
        else if (num2 > num1 && num2 > num3)
            System.out.println("Number 2 (" + num2 + ") is largest.\n");
        else if (num3 > num1 && num3 > num2)
            System.out.println("Number 3 (" + num3 + ") is largest.\n");
    }
}
