public class Methods {
    public static void getRequirements() {
        System.out.println("\nDeveloper: Michael Proeber\nProgram loops through array of strings.\nUse following values: dog, cat, bird, fish, insect\nUse following loop structures: for, enhanced for, while, do while\n\nPretest loops: for, enhanced for, while\nPosttest loops: do while");
    }

    public static void arrayLoops() {
        // can declare String array
        // String[] animals = new String[5];
        String animals[] = {"dog", "cat", "bird", "fish", "insect"};

        System.out.println("\nfor loop:");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i]);
        }

        // Enhanced for loop, or for-each loop, doesn't require an iterator; iterates through each element of an array
        System.out.println("\nenhanced for loop/for-each loop: ");
        for (String animal : animals) {
            System.out.println(animal);
        }

        // while loop
        int i = 0;
        System.out.println("\nwhile loop: ");
        while (i < animals.length) {
            System.out.println(animals[i]);
            i++;
        }

        // Do-while loop
        i = 0;
        System.out.println("\ndo-while loop: ");
        do {
            System.out.println(animals[i]);
            i++;
        } while (i < animals.length);
        System.out.println();
    }
}
