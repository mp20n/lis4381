/* Arrays are container objects that hold a fixed number of elements of a single data type.
After creation, its length is fixed

Array vs ArrayList:
1) Resizable
Array – fixed length that cannot change after creation
ArrayList – dynamic length

2) Data types
Array – contains both primitive data types (int, float, double) and objects
ArrayList – cannot contain primitive data types, only objects

3) Access and modification
Array – members accessed using []
ArrayList – set of methods to access and modify elements

4) Multi-dimensional
Array – can be multi-dimentional
ArrayList – single-dimentional


*/

public class Main {
    public static void main(String[] args) {
        Methods.getRequirements();
        Methods.arrayLoops();
    }
}