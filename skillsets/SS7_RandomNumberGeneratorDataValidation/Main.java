public class Main {
    public static void main(String args[]) {
        Methods.getRequirements();

        // Java style String[] myArray
        // C++ style String myArray[]
        // call createArray() method in Methods class
        // returns initialized array, array size determined by user
        int[] userArray = Methods.createArray();

        // call generatePseudoRandomNumber() method, passing returned array above
        // prints pseudo-randomly generated numbers determined by number user input
        Methods.generatePseudoRandomNumbers(userArray);
    }
}
