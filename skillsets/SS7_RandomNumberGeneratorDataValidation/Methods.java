import java.util.Scanner;
import java.util.Random; // import class to produce pseudo-random numbers

public class Methods {
    public static void getRequirements() {
        // display operational messages
        System.out.println("\nDeveloper: Michael Proeber");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Program validates user input for integers greater than 0.");
        System.out.println("Print minimum and maximum integer values.");
        System.out.println("Use following loop structures: for, enhanced for, while, do while.");

        System.out.println("\nInteger.MIN_VALUE = " + Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE);
    }

    public static int[] createArray() {
        Scanner sc = new Scanner(System.in);
        int arraySize = 0;

        // prompt user for number of randomly generated numbers
        System.out.print("\nEnter desired number of pseudorandom integers (min. 1): ");

        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!");
            sc.next();
            System.out.print("\nPlease try again. Enter valid integer (min 1): ");
        }
        arraySize = sc.nextInt();

        while (arraySize < 1) {
            System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");
            while (!sc.hasNextInt()) {
                System.out.print("Number must be integer: ");
                sc.next();
                System.out.print("Please try again. Enter integer greater than 0: ");
            }
            arraySize = sc.nextInt();
        }

        // Java style int[] myArray
        // C++ style int myArray[]
        int yourArray[] = new int[arraySize];
        return yourArray;
    }

    // method accepts int array
    public static void generatePseudoRandomNumbers(int[] myArray) {
        Random r = new Random(); // initialize random object variable

        // create loops to reandomize integer values
        int i = 0; // initialize counter variable
        System.out.println("\nFor loop:");
        for (i = 0; i < myArray.length; i++) {
            // generate random values from 0-9, excluding 10
            // int upperbound = 10;
            // int int_random = rand.nextInt(upperbound);

            // int x = r.nextInt(10) + 1; // generate number between 1 and 10, inclusive

            // generate random integer within Integer.MIN_VALUE and Integer.MAX_VALUE
            System.out.println(r.nextInt());
            // System.out.println(r.nextInt(10) + 1); // print pseudorandom number between 1 and 10, inclusive
        }

        System.out.println("\nEnhanced for loop:");
        for (int n : myArray) {
            System.out.println(r.nextInt());
        }

        System.out.println("\nWhile loop:");
        i = 0; // reassign to 0
        while (i < myArray.length) {
            System.out.println(r.nextInt());
            i++;
        }

        i = 0; // reassign to 0
        System.out.println("\nDo...while loop:");
        do {
            System.out.println(r.nextInt());
            i++;
        } while (i < myArray.length);
        System.out.println();
    }
}
