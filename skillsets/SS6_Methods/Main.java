class Main {
    public static void main(String args[]) {
        // call static void method (i.e., no object, non-value returning)
        Methods.getRequirements();
        Methods.getUserInput();
    }
}
