public class Main {
    public static void main(String[] args) {
        // call static void method
        Methods.getRequirements();
        Methods.largestNumber();

        // or call static value-returning method and void method

        int int1 = 0, int2 = 0;
        System.out.print("Enter first integer: ");
        int1 = Methods.getNum();

        System.out.print("Enter second integer: ");
        int2 = Methods.getNum();

        Methods.evaluateNumbers(int1, int2);
    }
}