import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("\nDeveloper: Michael Proeber\nProgram evaluates largest of 2 integers.\nNote: Program does not check for non-numeric characters or non-integer values.\n");
    }

    public static void largestNumber() {
        int int1, int2;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first integer: ");
        int1 = sc.nextInt();

        System.out.print("Enter second integer: ");
        int2 = sc.nextInt();

        if (int1 > int2) {
            System.out.println("\n" + int1 + " is larger than " + int2 + "\n");
        } else if (int2 > int1) {
            System.out.println("\n" + int2 + " is larger than " + int1 + "\n");
        } else {
            System.out.println("Integers are equal.\n");
        }
    }
}
