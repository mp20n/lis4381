/*

Array vs. ArrayList
1) Resizable
Array – static, fixed length
ArrayList – dynamic

2) Data types
Array – contain both primitive data types (int, float, double) and objects
ArrayList – cannot contain primitive data types, only objects

3) Access and modify data
Array – members accessed using []
ArrayList – set of methods to access elements and modify them (add(), remove(), etc)

4) Multi-dimensional
Array – can be multi-dimensional
ArrayList – only single-dimensional

*/

import java.util.Scanner;

public class Methods {
    // global Scanner object used in more than one method
    // final prevents object variable from being modified
    static final Scanner sc = new Scanner(System.in);

    public static void getRequirements() {
        // display operational messages
        System.out.println("\nDeveloper: Michael Proeber");
        System.out.println("1) Program creates array size at runtime.");
        System.out.println("2) Program displays array size.");
        System.out.println("3) Program rounds sum and average of numbers to 2 decimal places.");
        System.out.println("Numbers must be float data type, not double.");
    }

    public static int validateArraySize() {
        int arraySize = 0;

        System.out.print("\nPlease enter array size: ");
        while (!sc.hasNextInt()) {
            System.out.println("Not valid integer!");
            sc.next();
            System.out.print("\nPlease try again. Enter array size: ");
        }
        arraySize = sc.nextInt();
        
        return arraySize;
    }

    public static void calculateNumbers(int arraySize) {
        float sum = 0.0f;
        float average = 0.0F;

        System.out.println("\nPlease enter " + arraySize + " numbers.");

        float numsArray[] = new float[arraySize];

        for (int i = 0; i < arraySize; i++) {
            System.out.print("Enter num " + (i + 1) + ": ");

            while (!sc.hasNextFloat()) {
                System.out.println("Not valid number!");
                sc.next();
                System.out.print("\nPlease try again. Enter num " + (i + 1) + ": ");
            }
            numsArray[i] = sc.nextFloat();
            sum += numsArray[i];
        }

        average = sum/arraySize;

        System.out.print("\nNumbers entered: ");
        for (int i = 0; i < numsArray.length; i++)
            System.out.print(numsArray[i] + " ");

        printNumbers(sum, average);
    }

    public static void printNumbers(float sum, float average) {
        System.out.println("\nSum: " + String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f", average));
        System.out.println();
    }
}
