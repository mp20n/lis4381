public class Main {
    public static void main(String args[]) {
        Methods.getRequirements();

        int arraySize = 0;
        arraySize = Methods.validateArraySize();

        // print array values for testing purposes
        /*
        for (int i = 0; i < userArray.length; i++)
            System.out.print(userArray[i] + ", ");
        */

        Methods.calculateNumbers(arraySize);
    }
}
