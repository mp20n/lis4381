import java.util.Scanner;

public class Methods {
    public static void getRequirements() {
        System.out.println("\nDeveloper: Michael Proeber\nProgram evaluates integers as even or odd.\nNote: Program does not check for non-numeric characters.\n");
    }

    public static void evaluateNumber() {
        int integer = 0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter integer: ");
        integer = sc.nextInt();

        if (integer % 2 == 0) {
            System.out.println("\n" + integer + " is an even number.\n");
        } else {
            System.out.println("\n" + integer + " is an odd number.\n");
        }
    }
}
