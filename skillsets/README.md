# LIS4381 – Mobile Web App Development

## Michael Proeber

### Skillset 1 – Calculate Even or Odd

![Skillset 1](SS1_EvenOrOdd/SS1.png)

### Skillset 2 – Find the Largest Number

![Skillset 2](SS2_LargestNumber/SS2.png)

### Skillset 3 – Arrays and Loops

![Skillset 3](SS3_ArraysAndLoops/SS3.png)

### Skillset 4 – Decision Structures

![Skillset 4](SS4_DecisionStructures/SS4.png)

### Skillset 5 – Random Number Generator

![Skillset 5](SS5_RandomNumberGenerator/SS5.png)

### Skillset 6 – Methods

![Skillset 6](SS6_Methods/SS6.png)

### Skillset 7 – Random Number Generator with Data Validation

![Skillset 7](SS7_RandomNumberGeneratorDataValidation/SS7.png)

### Skillset 8 – Largest Three Numbers

![Skillset 8](SS8_LargestThreeNumbers/SS8.png)

### Skillset 9 – Array Runtime Data Validation

![Skillset 9](SS9_ArrayRuntimeDataValidation/SS9.png)

### Skillset 10 – Array List

![Skillset 10](SS10_ArrayList/SS10.png)

### Skillset 11 – Alphanumeric and Special Characters

![Skillset 11](SS11_AlphaSpecialCharacters/SS11.png)

### Skillset 12 – Temperature Conversion

![Skillset 12](SS12_TemperatureConversion/SS12.png)
