<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
		<meta name="author" content="Michael Proeber">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>
		
		<style>
			body
{
  padding-top: 50px;
}

.starter-template
{
  padding: 0px 15px;
  text-align: center;
}

h1
{
	margin: 0px;     
	color: #9A2A2A;
	text-shadow: 2px 2px #CEB888;
	padding: 0px;
	font-size: 48px;
	font-family: "trebuchet ms", sans-serif;    
}

h2 {
	font-weight: normal;
}

h3 {
	font-weight: normal;
}

.navbar-custom {
	background-color:#9A2A2A !important;
}

.navbar-custom .navbar-text {
	color: white;
}

.center {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}

th, td {
	padding: 15px;
}
	</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Learning how to develop Android apps is a valuable skill that developers can add to their skillset. Part of the development process is learning JavaScript to make interactable elements such as buttons, checkboxes, and scrollviews. Another important aspect is testing the app on multiple platforms, new and old. Compatibility for older software should be a high priority.
				</p>

				<br>

				<h4>GIF demos of bruschetta recipe application</h4>
				<table class="center">
					<tr>
						<th class="center">Nexus 4 API 22 Lollipop</th>
						<th class="center">Pixel 2 API 27 Oreo</th>
					</tr>
					<tr>
						<td><img src="img/Nexus_4_demo.gif" class="img-responsive center-block" alt="Nexus 4 demo"></td>
						<td><img src="img/Pixel_2_demo.gif" class="img-responsive center-block" alt="Pixel 2 demo"></td>
					</tr>
				</table>

				<br>

				<h4>Java skillsets</h4>
				<table class="center">
					<tr>
						<th class="center">Skillset 1 – EvenOrOdd</th>
						<th class="center">Skillset 2 – LargestNumber</th>
						<th class="center">Skillset 3 – ArraysAndLoops</th>
					</tr>
					<tr>
						<td><img src="img/SS1.png" class="img-responsive center-block" alt="Skillset 1"></td>
						<td><img src="img/SS2.png" class="img-responsive center-block" alt="Skillset 2"></td>
						<td><img src="img/SS3.png" class="img-responsive center-block" alt="Skillset 3"></td>
					</tr>
				</table>

				<br><br>

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
