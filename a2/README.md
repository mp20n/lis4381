# LIS4381 – Mobile Web App Development

## Michael Proeber

### Assignment #2 Requirements:

1. Develop an application that:
    - includes instructions for a recipe
    - includes a photo of the recipe
    - has a button
    - has a background color
    - has text color
2. Develop Skillset 1 – Even or Odd Number calculator
3. Develop Skillset 2 – Largest of Two Integers calculator
4. Develop Skillset 3 – Arrays and Loops demonstration
5. Chapter questions
6. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of recipe application running
* Screenshots of skillsets running
* Bitbucket repo links (course and this assignment)

#### Assignment Screenshots:

*GIF demos of bruschetta recipe application*

| Nexus 4 API 22 Lollipop | Pixel 2 API 27 Oreo |
| ----- | ------ |
| ![Nexus 4 demo using Android Lollipop](img/Nexus_4_demo.gif) | ![Pixel 2 demo using Android Oreo](img/Pixel_2_demo.gif) |

<br>

*Java skillsets*

| [Skillset 1 – EvenOrOdd](https://bitbucket.org/mp20n/lis4381/src/master/skillsets/SS1_EvenOrOdd/ "Skillset 1 link") | [Skillset 2 – LargestNumber](https://bitbucket.org/mp20n/lis4381/src/master/skillsets/SS2_LargestNumber/ "Skillset 2 link") | [Skillset 3 – ArraysAndLoops](https://bitbucket.org/mp20n/lis4381/src/master/skillsets/SS3_ArraysAndLoops/ "Skillset 3 link") |
| --------- | ------ | ------ |
| [![Skillset 1 screenshot](img/SS1.png)](img/SS1.png) | [![Skillset 2 screenshot](img/SS2.png)](img/SS2.png) | [![Skillset 3 screenshot](img/SS3.png)](img/SS3.png) |

<br>

#### Bitbucket Links:

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course repository")

*Assignment directory:*
[A2 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/a2/ "A2 Repository Directory")