<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Portfolio for Mobile Web App Development course at Florida State University with Mark Jowett">
		<meta name="author" content="Michael Proeber">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>
		
		<style>
			body
{
  padding-top: 50px;
}

.starter-template
{
  padding: 0px 15px;
  text-align: center;
}

h1
{
	margin: 0px;     
	color: #9A2A2A;
	text-shadow: 2px 2px #CEB888;
	padding: 0px;
	font-size: 48px;
	font-family: "trebuchet ms", sans-serif;    
}

h2 {
	font-weight: normal;
}

h3 {
	font-weight: normal;
}

.navbar-custom {
	background-color:#9A2A2A !important;
}

.navbar-custom .navbar-text {
	color: white;
}

.center {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}

th, td {
	padding: 15px;
}
	</style>
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Developing applications that can calculate input data on the fly is critical for advanced Android development. Additionally, app icons are an important aspect to having an interactive, presentable app. This assignment employs both and incorporates skills from past assignments.
				</p>

				<br>

				<h4>GIF demos of ticket calculator application</h4>
				<table class="center">
					<tr>
						<th class="center">Nexus 4 API 22 Lollipop</th>
						<th class="center">Pixel 2 API 27 Oreo</th>
					</tr>
					<tr>
						<td><img src="img/Nexus_4_demo.gif" class="img-responsive center-block" alt="Nexus 4 demo"></td>
						<td><img src="img/Pixel_2_demo.gif" class="img-responsive center-block" alt="Pixel 2 demo"></td>
					</tr>
				</table>

				<br>

				<h4>Java skillsets</h4>
				<table class="center">
					<tr>
						<th class="center">Skillset 4 – DecisionStructures</th>
						<th class="center">Skillset 5 – RandomNumberGenerator</th>
						<th class="center">Skillset 6 – Methods</th>
					</tr>
					<tr>
						<td><img src="img/SS4.png" class="img-responsive center-block" alt="Skillset 4"></td>
						<td><img src="img/SS5.png" class="img-responsive center-block" alt="Skillset 5"></td>
						<td><img src="img/SS6.png" class="img-responsive center-block" alt="Skillset 6"></td>
					</tr>
				</table>

				<br><br>

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
