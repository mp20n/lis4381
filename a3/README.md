# LIS4381 – Mobile Web App Development

## Michael Proeber

### Assignment #3 Requirements

1. Develop an application that:
    - calculates ticket cost based on a dropdown of differently priced concerts
    - has a custom launch icon that is displayed in the activity
    - has colors for activity controls
    - has borders around image and button
    - has text shadow in button
2. Develop Skillset 4 – Decision Structures
3. Develop Skillset 5 – Random Number Generator
4. Develop Skillset 6 – Methods
5. Chapter questions
6. Create Entity Relationship Diagram of pet store MySQL database with data

#### README.md file should include the following items

- Screenshot of Android ticket calculator application running
- Screenshots of skillsets running
- Screenshots of ERD and populated tables
- Links to A3 SQL and ERD files
- Bitbucket repo links (course and this assignment)

#### Assignment Screenshots

##### GIF demos of ticket calculator application

| Nexus 4 API 22 Lollipop | Pixel 2 API 27 Oreo |
| ----- | ------ |
| ![Nexus 4 demo using Android Lollipop](img/Nexus_4_demo.gif) | ![Pixel 2 demo using Android Oreo](img/Pixel_2_demo.gif) |

<br>

##### Java Skillsets

| [Skillset 4 – DecisionStructures](../skillsets/SS4_DecisionStructures/ "Skillset 4 link") | [Skillset 5 – RandomNumberGenerator](../skillsets/SS5_RandomNumberGenerator/ "Skillset 5 link") | [Skillset 6 – Methods](../skillsets/SS6_Methods/ "Skillset 6 link") |
| --------- | ------ | ------ |
| [![Skillset 4 screenshot](img/SS4.png)](img/SS4.png "Skillset 4") | [![Skillset 5 screenshot](img/SS5.png)](img/SS5.png "Skillset 5") | [![Skillset 6 screenshot](img/SS6.png)](img/SS6.png "Skillset 6") |

<br>

##### [ERD](docs/ERD.mwb "ERD .mwb file")

[![ERD image](img/ERD.png)](img/ERD.png "ERD")

<br>

##### SQL tables data

| Pet Store | Pet | Customer |
| ----- | ------ | ----- |
| [![Pet store data](img/petstore.png)](img/petstore.png) | [![Pet table data](img/pet.png)](img/pet.png) | [![Customer data](img/customer.png)](img/customer.png) |

<br>

#### Database File Links

*Forward-engineered SQL code:*
[SQL Code](docs/a3.sql "A3 SQL")

*Entity Relationship Diagram:*
[ERD .mwb](docs/a3.mwb "A3 .mwb file containing ERD")

#### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course Repository")

*Assignment directory:*
[A3 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/a3/ "A3 Repository Directory")