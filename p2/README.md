# LIS4381 – Mobile Web App Development

## Michael Proeber

### Project #2 Requirements

1. Create a form page to edit a pet store record in the petstore table, with server-side form validation
2. Create script to process edit query
3. Create script to process delete query
4. Create an RSS feed page displaying a live feed
5. Chapter questions

### README.md file should include the following items

- Screenshot of homepage index.php
- Screenshot of P2 index.php showing table before changes
- Screenshot of edit_petstore.php form
- Screenshot of failed server-side validation
- Screenshot of passed validation (after edit)
- Screenshot of delete prompt
- Screenshot of records after delete
- Screenshot of RSS feed
- Bitbucket repo links (course and this assignment)

### Project Screenshots

#### Homepage index.php

[![Homepage](img/homepage.png)](img/homepage.png)

<br> <br>

#### P2 index.php with table before changes

[![P2 index.php](img/index.png)](img/index.png)

<br> <br>

#### Edit form and process

| Edit form | Failed server-side validation | Passed validation (after edit) |
| --------- | ------ | ------ |
| [![Edit form](img/form.png)](img/form.png "Edit form") | [![Server-side validation](img/error.png)](img/error.png "Server-side validation") | [![Passed validation (after edit)](img/after_edit.png)](img/after_edit.png "Passed validation (after edit)") |

<br> <br>

#### Delete prompt and process

| Delete prompt | After record deletion |
| --------- | ------- |
| [![Delete prompt](img/prompt.png)](img/prompt.png "Delete prompt") | [![After record deletion](img/after_delete.png)](img/after_delete.png "After record deletion") |

<br> <br>

#### RSS feed

[![RSS feed](img/rss.png)](img/rss.png)

<br>

### Bitbucket Links

*Course repo:*
[Course Repository](https://bitbucket.org/mp20n/lis4381/src/master/ "Course Repository")

*Assignment directory:*
[P2 Repository Directory](https://bitbucket.org/mp20n/lis4381/src/master/p2/ "P2 Repository Directory")